(push (expand-file-name "init-shrimplets" user-emacs-directory) load-path)
(push (expand-file-name "shrimp-themes" user-emacs-directory) load-path)
;(setq gnutls-algorithm-priority "NORMAL:-VERS-TLS1.3")

(load "init-packages")

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(ansi-color-names-vector
   ["#000000" "#8b0000" "#00ff00" "#ffa500" "#7b68ee" "#dc8cc3" "#93e0e3" "#dcdccc"])
 '(custom-enabled-themes (quote (vibrant-cyber)))
 '(custom-safe-themes
   (quote
    ("4532d188f2bc44377b15769d288e6787540a26fbff4ef3919e4deae5b9973db5" "b2a77e95e1ec8fa18a8a5a48ae13e40f3cc78c511347136c8e95853e5538578f" "bade6a07214eba094d203f1217915cedd267a8ecc887894a752990afa076837c" "2c805cc16d36412b312e10ee84df882728704bf4fd5bf88ca7e47dadfe95a0c3" "afe3b9265401e485112795d77d734075c9fcee93cdf58522f472cec8f0233695" "f8b2f0f0084c32ebf74b9ba4aa0216f6f3f966c2026d473478b63aca3a60f4b2" "addfaf4c6f76ef957189d86b1515e9cf9fcd603ab6da795b82b79830eed0b284" default)))
 '(package-selected-packages
   (quote
    (ag powershell xref-js2 which-key web-mode treemacs-evil systemd sr-speedbar smartparens slime-company rainbow-delimiters popwin lua-mode js2-refactor indent-guide helm-tramp helm-slime helm-projectile helm-gtags helm-descbinds groovy-mode groovy-imports ggtags flycheck evil-magit eval-in-repl eslint-fix elpy dockerfile-mode docker-tramp docker-compose-mode diff-hl dashboard cyberpunk-theme company-web company-tern all-the-icons))))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(load "init-themes")
(load (expand-file-name "shrimplet" user-emacs-directory))
