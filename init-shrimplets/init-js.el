(require 'init-packages)
(setq my-js-packages
      '(
	company
	js2-mode
	js2-refactor
	xref-js2 ; For better react syntax highlighting
	;company-tern
	company-web
	web-mode
	ggtags
	eslint-fix
	))
(mapc 'install-if-needed my-js-packages)


(require 'web-mode)
;(require 'company-web)
(add-hook 'js2-mode-hook
      (function (lambda ()
              (js2-imenu-extras-mode t)
              (js2-refactor-mode t)
              (tern-mode)
              (company-mode)
	      (hs-minor-mode 1)
	      (ggtags-mode 1)
	      (hs-minor-mode)
              (setq electric-pair-preserve-balance nil
		    electric-pair-mode 1
		    indent-tabs-mode nil
		    js2-basic-offset 2))))

(require 'js2-mode)
(require 'xref-js2)
(add-to-list 'auto-mode-alist '("\\.js\\'" . js2-mode))


(add-to-list 'auto-mode-alist '("\\.css\\'" . web-mode))
(add-to-list 'company-backends 'company-web)
(add-hook 'web-mode
      (function (lambda ()
		  (setq electric-pair-preserve-balance nil
			electric-pair-mode 1))))

(provide 'init-js)
;;; init-js ends here
