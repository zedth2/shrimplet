(require 'init-packages)
(setq my-proj-packages
      '(
	projectile
	helm-projectile
	))

(mapc 'install-if-needed my-proj-packages)
(require 'projectile)
(projectile-mode +1)
(define-key projectile-mode-map (kbd "C-q") 'projectile-command-map)
(define-key projectile-mode-map (kbd "C-c p") 'projectile-command-map)



(provide 'init-projectile)
