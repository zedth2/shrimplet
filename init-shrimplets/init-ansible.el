;;; init-ansible --- Provides Ansible configuration
;;; Commentary:
(require 'init-packages)

(setq my-ansible-packages
      '(
	ansible
	ansible-doc
	ansible-vault
	company-ansible
	))
(mapc 'install-if-needed my-ansible-packages)

(add-hook 'ansible-hook (lambda ()
			  (whitespace-mode t)
			  (hs-minor-mode 1)
			  (highlight-indentation-mode 1)
			  (company-mode)))

(provide 'init-ansible)
