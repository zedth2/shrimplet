(require 'init-packages)
(setq my-tree-packages
      '(lv
	treemacs
	treemacs-evil
	ggtags
	))
(mapc 'install-if-needed my-tree-packages)
(require 'lv)
(require 'treemacs)
(require 'treemacs-evil)
(require 'ggtags)
(global-set-key (kbd "<f8>") 'treemacs)
(setq
 treemacs-collapse-dirs 1
 treemacs-no-png-images t)

(setq-local imenu-create-index-function #'ggtags-build-imenu-index)
(ggtags-mode)



(provide 'init-treemacs)
