(require 'init-packages)
(setq my-dash-packages
      '(dashboard
	))

(mapc 'install-if-needed my-dash-packages)

(require 'dashboard)
(dashboard-setup-startup-hook)

(setq dashboard-items '((recents . 20)
			(bookmarks . 10)
			(projects . 15)
			(registers . 10)))

;(setq dashboard-startup-banner 'offical)
(provide 'init-dashboard)
