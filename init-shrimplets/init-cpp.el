(require 'init-packages)
(setq my-cpp-packages
      '(
	function-args
	company
	cc-mode
	))

(provide 'init-cpp)
