(require 'init-packages)
(setq my-groovy-packages
      '(groovy-mode
	groovy-imports
	))
(mapc 'install-if-needed my-groovy-packages)


(provide 'init-groovy)
