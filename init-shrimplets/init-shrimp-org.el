(require 'init-packages)

(require 'org)
(setq my-org-packages
      '(
	org-bullets
	org-ac))
(mapc 'install-if-needed my-org-packages)
(require 'org-ac)
(require 'org-bullets)

(add-hook 'org-mode-hook
	  (function
	   (lambda ()
	     (org-bullets-mode)
	     (org-indent-mode)
	     (auto-complete-mode)
	     (define-key ac-mode-map (kbd "M-TAB") 'auto-complete)
	     (org-ac/config-default)
	     (add-to-list 'ac-modes 'org-mode))))


	     ;(define-key ac-mode-map (kbd "M-TAB") 'auto-complete)
(provide 'init-shrimp-org)
;;; init-org-shrimp ends here
