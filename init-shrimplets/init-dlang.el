(require 'init-packages)


(setq my-dlang-packages
      '(ivy
	popwin
	yasnippet
	d-mode
	flycheck-dmd-dub))

(mapc 'install-if-needed my-dlang-packages)

(require 'company-dcd)
(add-hook 'd-mode-hook (lambda ()
			 (flycheck-dmd-dub-set-variables)
			 (company-dcd-mode)))

(provide 'init-dlang)
