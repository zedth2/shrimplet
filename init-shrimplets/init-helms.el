(require 'init-packages)
(setq my-helm-packages
      '(helm
	helm-descbinds
	helm-gtags
	helm-tramp
	)
      )
(mapc 'install-if-needed my-helm-packages)

(require 'helm)
(helm-mode 1)

(define-key helm-map (kbd "<tab>") 'helm-execute-persistent-action)

(global-set-key (kbd "M-x") #'helm-M-x)
(global-set-key (kbd "C-x C-f") #'helm-find-files)
(global-set-key (kbd "C-x b") #'helm-mini)
(global-set-key (kbd "C-s") #'swiper-helm)
(global-set-key (kbd "M-y") #'helm-show-kill-ring)

(require 'helm-descbinds)
(setq helm-descbinds-window-style 'split)
(add-hook 'helm-mode-hook 'helm-descbinds-mode)


(provide 'init-helms)
