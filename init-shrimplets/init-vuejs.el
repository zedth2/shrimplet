(require 'init-packages)
(require 'init-js)

(setq my-vuejs-packages
      '(
	vue-mode
	vue-html-mode
	))
(mapc 'install-if-needed my-vuejs-packages)

(require 'web-mode)
(add-hook 'js-mode-hook
      (function (lambda ()
              ;(js2-imenu-extras-mode t)
              ;(js2-refactor-mode t)
              (tern-mode)
              (company-mode)
	      (hs-minor-mode 1)
	      (ggtags-mode 1)
	      (hs-minor-mode)
              (setq electric-pair-preserve-balance nil
		    electric-pair-mode 1
		    indent-tabs-mode nil
		    js-indent-level 2))))
(add-hook 'mmm-mode-hook
	  (function
	   (lambda ()
	     (setq mmm-js-mode-enter-hook (lambda () (setq syntax-ppss-table nil))
		   mmm-typescript-mode-enter-hook (lambda (setq syntax-ppss-table nil))))))

(require 'js-mode)
