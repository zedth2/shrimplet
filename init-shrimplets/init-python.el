(require 'init-packages)
(setq my-py-packages
      '(
	python-mode
	company
	jedi
	company-jedi
	ggtags
	))
(mapc 'install-if-needed my-py-packages)
;(load "~/.emacs.d/emacs-jedi-direx/jedi-direx")
(autoload 'python-mode "python-mode" "Python Mode." t)
(add-to-list 'auto-mode-alist '("\\.py\\'" . python-mode))
(add-hook 'python-mode-hook
      (function (lambda ()
           (setq indent-tabs-mode nil
             tab-width 4
             jedi:complete-on-dot t
             )
           (eldoc-mode -1)
           (jedi:setup)
           (whitespace-mode t)
           (add-to-list 'company-backends 'company-jedi)
           (eval-after-load "python" '(define-key python-mode-map "\C-cx" 'jedi-direx:pop-to-buffer))
           (add-hook 'jedi-mode-hook 'jedi-direx:setup)
	   (ggtags-mode 1)
	   (hs-minor-mode 1)
           )))

(provide 'init-python)
;;; init-python ends here
