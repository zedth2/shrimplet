(require 'init-packages)
(setq my-docker-packages
      '(
	docker-compose-mode
	dockerfile-mode
	docker-tramp
	))
(mapc 'install-if-needed my-docker-packages)

(provide 'init-docker)
