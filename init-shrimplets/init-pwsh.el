(require 'init-packages)
(setq my-pwsh-packages
      '(powershell)
      )

(mapc 'install-if-needed my-pwsh-packages)
(add-to-list 'auto-mode-alist '("\\.psm1\\'" . powershell-mode))

(provide 'init-pwsh)
