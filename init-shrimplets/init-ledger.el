

(require 'calendar)

(defun insdate-insert-current-date (&optional omit-day)
  ""
  (interactive "P*")
  (insert (calendar-date-string (calendar) nil omit-day)))

(add-to-list 'auto-mode-alist '("\\.ledger$" "\\.dat$" . ledger-mode))
(add-hook 'ledger-mode-hook 'goto-address-prog-mode)


(provide 'init-ledger)
