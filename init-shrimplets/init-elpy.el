(require 'init-packages)
(setq my-elpy-packages
      '(elpy)
      )
(mapc 'install-if-needed my-elpy-packages)
(elpy-enable)

(add-hook 'elpy-mode-hook (lambda ()
			    (whitespace-mode t)
			    (hs-minor-mode 1)
			    (highlight-indentation-mode 1)))
(define-key elpy-mode-map (kbd "C-c .") 'elpy-goto-assignment)
;(add-hook 'python-mode-hook
;	  (function (lambda ()

(provide 'init-elpy)
