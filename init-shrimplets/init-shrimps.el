(require 'init-packages)

(defun date-today ()
  (interactive)
  (insert (format-time-string "%Y-%m-%d")))


(provide 'init-shrimps)
;;; init-shrimps.el ends here
