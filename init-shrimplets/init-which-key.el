(require 'init-packages)

(setq my-which-key-packages
      '(which-key
	)
      )
(mapc 'install-if-needed my-which-key-packages)
(require 'which-key)
(which-key-mode)

(provide 'init-which-key)
