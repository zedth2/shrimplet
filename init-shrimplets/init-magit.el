(require 'init-packages)
(setq my-magit-packages
      '(
	magit
	evil-magit
	))
(mapc 'install-if-needed my-magit-packages)

(require 'magit)
(require 'evil-magit)

(provide 'init-magit)
