(require 'init-packages)

(setq my-slime-packages
      '(slime
	slime-company
	eval-in-repl
	helm-slime))

(mapc 'install-if-needed my-slime-packages)

(require 'slime)
(require 'eval-in-repl)
(require 'eval-in-repl-slime)

(add-hook 'lisp-mode-hook
	  (lambda () (local-set-key (kbd "<C-return>") 'eir-eval-in-slime)))

(setq slime-contribs '(slime-fancy))

(provide 'init-slime)
