(require 'init-packages)
(setq my-lua-packages
      '(lua-mode
	))

;(mapc 'install-if-needed my-lua-packages)
(add-to-list 'load-path "/home/zac/.emacs.d/pulls/lua-mode")
(autoload 'lua-mode "lua-mode" "Lua editing mode." t)
(add-to-list 'auto-mode-alist '("\\.lua$" . lua-mode))
(add-to-list 'interpreter-mode-alist '("lua" . lua-mode))

(provide 'init-lua)
