(require 'init-packages)
(setq my-packages
      '(
        company
;        rainbow-mode
        rainbow-delimiters
        smartparens
        all-the-icons
        sr-speedbar
        diff-hl
        flycheck
        hl-line
	popwin
	evil
	indent-guide
	cyberpunk-theme
	dashboard
	systemd
    flycheck
    ledger-mode
    ))
(mapc 'install-if-needed my-packages)
(require 'evil)
(evil-mode 1)
(require 'init-treemacs)
(global-set-key (kbd "<f8>") 'treemacs)
(require 'init-helms)
(require 'init-projectile)
(require 'init-dashboard)
(require 'init-which-key)
(require 'init-shrimps)
(require 'init-ledger)
(require 'init-dlang)
;(load "~/.emacs.d/transpose-frame")
;(require 'transpose-frame)
(mapc 'install-if-needed my-packages)
;(require 'popwin)
;(push '(direx:direx-mode :position left :width 25 :dedicated t)
;      popwin:special-display-config)
;(global-set-key (kbd "C-x C-j") 'direx:jump-to-directory-other-window)
;(require 'all-the-icons)
;(load "~/.emacs.d/sidebar.el/sidebar")
;(require 'sidebar)

(add-hook 'after-init-hook (lambda ()
			     (show-paren-mode t)
			     (global-company-mode)
			     (rainbow-delimiters-mode 1)
					;(highlight-changes-mode nil)
			     (global-diff-hl-mode 1)
			     (hl-line-mode 1)
			     (ido-mode 1)
			     (indent-guide-global-mode)
			     (column-number-mode t)))

(global-set-key (kbd "C-SPC") 'company-complete)

(add-hook 'before-save-hook
	  (lambda ()
	    (delete-trailing-whitespace)))

(add-hook 'package-menu-mode-hook
	  (lambda ()
	    (local-set-key (kbd "C-s") #'isearch-forward)
	    (local-set-key (kbd "C-r") #'isearch-reverse)))

					;(setq diff-hl-change ((t (:background "#46D9FF"))))
					;  (setq diff-hl-delete ((t (:background "#ff6c6b"))))
					;  (setq diff-hl-insert ((t (:background "#98be65"))))
					;(require 'sr-speedbar)

					;(global-set-key [f8] 'sr-speedbar-toggle)
(setq neo-theme (if (display-graphic-p) 'icons 'arrow))
(setq backup-directory-alist `(("." . "~/.emacs.d/backups")))
(setq delete-old-versions t)
(setq version-control t)
(setq vc-make-backup-files t)
(setq vc-follow-symlinks t)
(setq inferior-lisp-program "/usr/bin/sbcl")
(setq tramp-default-method "ssh")
(setq electric-pair-pairs '(
			    (?\" . ?\")
			    (?\{ . ?\})))

(add-hook 'after-init-hook
	  (lambda ()
	    (tool-bar-mode -1)
	    (global-flycheck-mode)
	    (global-company-mode)))
;(show-paren-mode t)
(global-linum-mode t)
(recentf-mode 1)
(setq recentf-max-menu-items 25)
(setq recentf-max-saved-items 25)
(global-set-key "\C-x\ \C-r" 'recentf-open-files)
(global-set-key "\C-c \C-j" 'counsel-imenu)

;(rainbow-delimiters-mode)
;(highlight-changes-mode)
;(evil-define-key 'normal neotree-mode-map (kbd "TAB") 'neotree-enter)
;(evil-define-key 'normal neotree-mode-map (kbd "SPC") 'neotree-quick-look)
;(evil-define-key 'normal neotree-mode-map (kbd "q") 'neotree-hide)
;(evil-define-key 'normal neotree-mode-map (kbd "RET") 'neotree-enter)
;(evil-define-key 'normal neotree-mode-map (kbd "g") 'neotree-refresh)
;(evil-define-key 'normal neotree-mode-map (kbd "n") 'neotree-next-line)
;(evil-define-key 'normal neotree-mode-map (kbd "p") 'neotree-previous-line)
;(evil-define-key 'normal neotree-mode-map (kbd "A") 'neotree-stretch-toggle)
;(evil-define-key 'normal neotree-mode-map (kbd "H") 'neotree-hidden-file-toggle)
;(setq scroll-margin 40)
(setq scroll-up-aggressively 1.0)
(setq scroll-down-aggressively 0.75)

(require 'init-elpy)
;(require 'init-python)
(setq elpy-rpc-python-command "python3")
(setq elpy-rpc-timeout 10)
;(require 'init-js)
;(require 'init-vuejs)
(require 'init-cpp)
(require 'init-docker)
(require 'init-magit)
(require 'init-groovy)
(require 'init-lua)
(require 'init-slime)
(require 'init-shrimp-org)
(require 'init-pwsh)
(require 'init-ansible)

;(load-theme 'cyberpunk)
(load-theme 'vibrant-cyber)

(provide 'shrimplet)
;;; shrimplet ends here
